#include "sha-256.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

// clang-format off
#define ROTRIGHT(a, b)  (((a) >> (b)) | ((a) << (32 - (b))))
#define CH(x, y, z)     (((x) & (y)) ^ (~(x) & (z)))
#define MAJ(x, y, z)    (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))
#define EP0(x)          (ROTRIGHT(x, 2) ^ ROTRIGHT(x, 13) ^ ROTRIGHT(x, 22))
#define EP1(x)          (ROTRIGHT(x, 6) ^ ROTRIGHT(x, 11) ^ ROTRIGHT(x, 25))
#define SIG0(x)         (ROTRIGHT(x, 7) ^ ROTRIGHT(x, 18) ^ ((x) >> 3))
#define SIG1(x)         (ROTRIGHT(x, 17) ^ ROTRIGHT(x, 19) ^ ((x) >> 10))
// clang-format on

static uint32_t K[64] = {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1,
                         0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
                         0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786,
                         0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
                         0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
                         0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
                         0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
                         0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
                         0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a,
                         0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
                         0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};

uint32_t **message2blocks(const char *msg, size_t len, size_t *block_count) {
    uint32_t **blocks = NULL;
    unsigned char *tmsg = NULL;
    size_t i = 0, j = 0;
    uint32_t b[4] = {};
    unsigned char c = 0;
    uint64_t bitlen = 0;

    bitlen = len * 8;

    tmsg = malloc(len + 1);

    // copy message to temporary space with '0b10000000' at the end
    memcpy(tmsg, msg, len);
    tmsg[len] = 0x80;
    len++;

    // calc number of blocks needed to contain msg + '0b10000000' + 64bit len of msg
    // each block is 512 (32*16) bits long
    *block_count = ceil((len / 4.0 + 2) / 16.0);
    blocks = malloc(*block_count * sizeof(uint32_t *));

    for (i = 0; i < *block_count; ++i) {
        blocks[i] = calloc(16, sizeof(uint32_t));
    }

    for (i = 0; i * 4 < len; ++i) {
        for (j = 0; j < 4; ++j) {
            c = (i * 4 + j < len ? tmsg[i * 4 + j] : 0);
            b[j] = c << (8 * (3 - j));
        }
        blocks[i / 16][i % 16] = b[0] | b[1] | b[2] | b[3];
    }

    blocks[*block_count - 1][14] = bitlen >> 32u;
    blocks[*block_count - 1][15] = (bitlen & 0xffffffff);

    free(tmsg);

    return blocks;
}

void sha_256(uint32_t hash[8], uint32_t **blocks, size_t block_count) {
    uint32_t H[] = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
                    0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};
    uint32_t w[64] = {};
    size_t i = 0, j = 0;
    uint32_t a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0;
    uint32_t t1 = 0, t2 = 0;

    for (i = 0; i < block_count; ++i) {
        memset(w, 0, sizeof(w));

        for (j = 0; j < 16; ++j) w[j] = blocks[i][j];
        for (j = 16; j < 64; ++j) w[j] = w[j - 16] + w[j - 7] + SIG0(w[j - 15]) + SIG1(w[j - 2]);

        a = H[0];
        b = H[1];
        c = H[2];
        d = H[3];
        e = H[4];
        f = H[5];
        g = H[6];
        h = H[7];

        for (j = 0; j < 64; ++j) {
            t1 = h + EP1(e) + CH(e, f, g) + K[j] + w[j];
            t2 = EP0(a) + MAJ(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + t1;
            d = c;
            c = b;
            b = a;
            a = t1 + t2;
        }

        H[0] += a;
        H[1] += b;
        H[2] += c;
        H[3] += d;
        H[4] += e;
        H[5] += f;
        H[6] += g;
        H[7] += h;
    }

    for (i = 0; i < 8; ++i) hash[i] = H[i];
}
