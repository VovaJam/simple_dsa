#pragma once

#include <stddef.h>
#include <stdint.h>

uint32_t **message2blocks(const char *msg, size_t len, size_t *block_count);
void sha_256(uint32_t hash[8], uint32_t **blocks, size_t block_count);
