#pragma once

#include <gmp.h>

typedef struct dsa_domain {
    gmp_randstate_t randstate;
    mpz_t p, q, g;
} dsa_domain_t;

typedef struct dsa_priv_key {
    mpz_t p, q, g;
    mpz_t key;
} dsa_priv_key_t;

typedef struct dsa_pub_key {
    mpz_t p, q, g;
    mpz_t key;
} dsa_pub_key_t;

typedef struct dsa_signature {
    mpz_t r, s;
} dsa_signature_t;

void dsa_domain_init(dsa_domain_t *dsad);
void dsa_domain_deinit(dsa_domain_t *dsad);
void dsa_domain_gen_params(dsa_domain_t *dsad);

void dsa_priv_key_init(dsa_priv_key_t *privkey);
void dsa_priv_key_deinit(dsa_priv_key_t *privkey);
void dsa_pub_key_init(dsa_pub_key_t *pubkey);
void dsa_pub_key_deinit(dsa_pub_key_t *pubkey);

void dsa_signature_init(dsa_signature_t *sign);
void dsa_signature_deinit(dsa_signature_t *sign);

void dsa_gen_keys(dsa_domain_t *dsad, dsa_priv_key_t *privkey, dsa_pub_key_t *pubkey);
void dsa_sign_message(dsa_signature_t *sign, dsa_priv_key_t *privkey, const char *msg, size_t len);
int dsa_validate_sign(dsa_signature_t *sign, dsa_pub_key_t *pubkey, const char *msg, size_t len);
